﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.InteropServices.ComTypes;
using System.Windows.Forms;
using OpcRcw.Comn;
using OpcRcw.Da;
using FILETIME = OpcRcw.Da.FILETIME;

namespace ScadaAlarms
{
    public partial class Form1 : Form
    {
        private static object _mPIfaceObject;
        private static int _mHGroup; // Описатель группы
        private int _mDwCookie;// Описатель подписки к событиям сервера
        public OpcRcw.Comn.IConnectionPoint MpDataCallback;// Точка подключения к событиям сервера
        public DataCallback MpSink;//Объект, который будет принимать вызовы сервера
        public object iFaceObj;
        private IntPtr iptrErrors;
        private IntPtr iptrResults;

        public void PaintReport(object[] newEventRow)
        {
            dataGridView1.Rows.Add(newEventRow);
        }

        public class DataCallback : IOPCDataCallback
        {
            private readonly Form1 _mDlg;

            //конструктор
            public DataCallback(Form1 dlg)
            {
                _mDlg = dlg; // Передаем ссылку на нашу форму в качестве параметра
            }

            //========Перегрузка методов интерфейса IOPCDataCallback ============//
            public void OnDataChange(int dwTransid, int hGroup, int hrMasterquality, int hrMastererror, int dwCount, int[] phClientItems, object[] pvValues, short[] pwQualities, FILETIME[] pftTimeStamps, int[] pErrors)
            {
                object[] row = new object[3];
                try
                {
                    var iptrValues = Marshal.AllocCoTaskMem(50); // выделение памяти
                    Marshal.GetNativeVariantForObject(pvValues, iptrValues); //Преобразует объект в COM VARIANT
                    var vt = new byte[50];
                    Marshal.Copy(iptrValues, vt, 0, 2);
                    foreach (var item in pvValues)
                    {
                        row[0] = item;
                        row[2] = (ToStringConverter.GetFtSting(pftTimeStamps));
                        _mDlg.SetItemValue(row);
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e);
                    throw;
                }

            }

            public void OnReadComplete(int dwTransid, int hGroup, int hrMasterquality, int hrMastererror, int dwCount, int[] phClientItems, object[] pvValues, short[] pwQualities, FILETIME[] pftTimeStamps, int[] pErrors)
            {
            }

            public void OnWriteComplete(int dwTransid, int hGroup, int hrMastererr, int dwCount, int[] pClienthandles, int[] pErrors)
            {
            }

            public void OnCancelComplete(int dwTransid, int hGroup)
            {
            }
        }


        class ToStringConverter
        {
            static public string GetFtSting(FILETIME[] ft)
            {

                long hFt2 = (((long)ft[0].dwHighDateTime) << 32) + ft[0].dwLowDateTime;
                DateTime dt = DateTime.FromFileTime(hFt2);
                return dt.ToString(CultureInfo.InvariantCulture);
            }
        }

        public void SetItemValue(object[] newEventRow)
        {
            try
            {
                if (dataGridView1.InvokeRequired)
                    dataGridView1.Invoke(new MethodInvoker(() => { PaintReport(newEventRow); }));
                else
                    PaintReport(newEventRow);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }



        public object ReadItem(string nameGroup, int dwCount, string[] accessPath, out object iFaceObj, out IntPtr iptrResults, out IntPtr iptrErrors)
        {
            var szItemID = new string[dwCount];
            var pServer = (IOPCServer)_mPIfaceObject;
            var updateRate = 3000; // частота опроса группы
            var riid = typeof(IOPCItemMgt).GUID;
            var timeBias = 0;
            float deadBand = 0;
            var bActive = 1;
            var hClientGroup = 1;
            try
            {
                pServer.AddGroup(nameGroup, bActive, updateRate, hClientGroup, (IntPtr)timeBias, (IntPtr)deadBand, 2, out _mHGroup, out updateRate, ref riid, out iFaceObj);
                var pItems = new OPCITEMDEF[dwCount];
                for (var j = 0; j < dwCount; j++)
                {
                    pItems[j] = new OPCITEMDEF { szItemID = szItemID[j] };
                    pItems[j].szItemID = accessPath[j];
                    pItems[j].szAccessPath = null;
                    pItems[j].bActive = 1;
                    pItems[j].hClient = 1;
                    pItems[j].vtRequestedDataType = (short)VarEnum.VT_EMPTY;
                    pItems[j].dwBlobSize = 0;
                    pItems[j].pBlob = IntPtr.Zero;
                }
                var pItemMgt = (IOPCItemMgt)iFaceObj;
                pItemMgt.AddItems(dwCount, pItems, out iptrResults, out iptrErrors);
                var hRes = new int[1];
                Marshal.Copy(iptrErrors, hRes, 0, 1);
                Marshal.ThrowExceptionForHR(hRes[0]);
                var pResults = (OPCITEMSTATE)Marshal.PtrToStructure(iptrResults, typeof(OPCITEMSTATE));
                Marshal.FreeCoTaskMem(iptrResults);
                Marshal.FreeCoTaskMem(iptrErrors);
                Marshal.Copy(iptrErrors, hRes, 0, 1);
                Marshal.ThrowExceptionForHR(hRes[0]);
                return iFaceObj;
            }
            catch (Exception ex)
            {
                string msg;
                var hRes = Marshal.GetHRForException(ex);
                pServer.GetErrorString(hRes, 2, out msg);
                MessageBox.Show(msg, @"Ошибка");
                throw;
            }
        }


        public bool ConnectServer()
        {
            try
            {
                var guid = new Guid("{9c68a3b7-21e4-4a9a-85d4-59b2caae4125}");
                var typeOfServer = Type.GetTypeFromCLSID(guid);
                _mPIfaceObject = Activator.CreateInstance(typeOfServer);
                return true;
            }
            catch (Exception ex)
            {
                var pServer = (IOPCServer)_mPIfaceObject;
                string msg;
                //Получаем HRESULT, соответствующий сгенерированному исключению
                var hRes = Marshal.GetHRForException(ex);
                //Запрашиваем у сервера текст ошибки`
                pServer.GetErrorString(hRes, 2, out msg);
                //Показываем сообщение ошибки
                MessageBox.Show(msg, @"Ошибка");
                return false;
            }
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            
        }

        public Form1()
        {
            InitializeComponent();

        }

        #region //запись на сервер

        private void AddItemToServer()
        {
            object iFaceObj;
            var Count = 1;
            var AccessPath = new string[Count];
            var pValueOn = new object[1];
            var pValueOff = new object[1];
            var hServers = new int[1];
            var hRes = new int[1];
            var pResults = new OPCITEMRESULT[1];
            var nameGroupKvitAll = "GroupWRITEITEM";
            var pServer = (IOPCServer)_mPIfaceObject;
            try
            {
                if (ConnectServer())
                {
                    AccessPath[0] = "Root.PLC.Application.NS.WRITEITEM";
                    ReadItem(nameGroupKvitAll, Count, AccessPath, out iFaceObj, out iptrResults, out iptrErrors);
                    pValueOn[0] = true;
                    pValueOff[0] = false;
                    pResults[0] = (OPCITEMRESULT)Marshal.PtrToStructure(iptrResults, typeof(OPCITEMRESULT));
                    hServers[0] = pResults[0].hServer;
                    var pSyncIO = (IOPCSyncIO)iFaceObj;
                    for (var i = 0; i < 10; i++)
                    {
                        pSyncIO.Write(1, hServers, pValueOn, out iptrErrors);
                        Marshal.Copy(iptrErrors, hRes, 0, 1);
                        Marshal.ThrowExceptionForHR(hRes[0]);
                    }
                    pSyncIO.Write(Count, hServers, pValueOff, out iptrErrors);
                    Marshal.Copy(iptrErrors, hRes, 0, 1);
                    Marshal.ThrowExceptionForHR(hRes[0]);
                }
            }
            catch (ApplicationException ex)
            {
                string msg;
                var hResult = Marshal.GetHRForException(ex);
                pServer.GetErrorString(hResult, 2, out msg);
                MessageBox.Show(msg, @"Ошибка");
            }
        }

        #endregion

        private void Form1_Load_1(object sender, EventArgs e)
        {
            try
            {
                if (ConnectServer())
                {
                    var pServer = (IOPCServer)_mPIfaceObject;
                    if (_mDwCookie != 0)
                        MpDataCallback.Unadvise(_mDwCookie);
                    if (_mHGroup != 0)
                    {
                        pServer.RemoveGroup(_mHGroup, 1);
                        _mHGroup = 0;
                    }
                    var nameGroup = "Group";
                    var Count = 100;
                    var AccessPath = new string[Count];
                    for (var i = 0; i < Count - 1; i++)
                        AccessPath[i] = "Root.PLC.Application.Example[" + (i + 1) + "]";
                    ReadItem(nameGroup, Count, AccessPath, out iFaceObj, out iptrResults, out iptrErrors);
                    var pCpc = (OpcRcw.Comn.IConnectionPointContainer)iFaceObj;
                    var riid = typeof(IOPCDataCallback).GUID;
                    pCpc.FindConnectionPoint(ref riid, out MpDataCallback); //Запрашивает, доступный для соединения объект, если он содержит точку подключения 
                    if (MpSink == null)
                        MpSink = new DataCallback(this);
                    MpDataCallback.Advise(MpSink, out _mDwCookie);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), @"Form1_Load Event Необходимо проверить OPC сервер");
                throw;
            }
        }
    }
}
